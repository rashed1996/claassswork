<x-backend.layouts.master>
    <h4 class="mt-4">Orders</h4>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">order edit</li>
        </ol>
    <form method="post"  action="{{route('order.update',['id'=>$order->id])}}">
    @csrf
    @method('patch')
      <input type="hidden" name="order_id" value="{{$order->id}}">
     <div class="mb-3">
     <label for="order_status" class="form-label"> <h5>Order Status:</h5></label>
    
    <select class="form-select" id="order_status" name="order_status">
    <option selected>Pending</option>
    <option>Delivered</option>
    </select>
     </div>
    
    <div class=" d-grid col-3 mx-auto mt-2">
    <button type="sumbit" class="btn btn-outline-info">Update Order</button>
    </div>
    
    
    
    </form>
    </x-backend.layouts.master>