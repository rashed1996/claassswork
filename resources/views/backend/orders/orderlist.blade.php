<x-backend.layouts.master>
    <h4 class="mt-4">Orders</h4>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">orders</li>
        </ol>
      <div class="container">
        @if(Session::has('message'))
        <p class="alert alert-danger">{{session::get('message')}}</p>
        @endif
        <table class="table table-striped table-hover table-info table-sm table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col">SL</th>
              <th scope="col">Order ID</th>
              <th scope="col">OrderBy</th>
              <th scope="col">Order Status</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Email </th>
              <th scope="col">Shipping Address </th>
              <th scope="col">Payment Method </th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            @php
            $sl=1;
            @endphp
            @foreach($orders as $order)
            <tr>
              <td>{{$sl++}}</td>
              <td>{{$order->id}}</td>
              <td>{{Auth::user()->name}}</td>
              <td>{{$order->status}}</td>
              <td>{{$order->phone_no}}</td>
              <td>{{$order->email}}</td>
              <td>{{ Str::limit($order->shipping_address,30)}}</td>
              <td>{{$order->payment_method}}</td>
              <td>
                <a class="btn btn-info" href="">Show</a>
                <a class="btn btn-primary" href="{{route('order-edit',['id'=>$order->id]??'1')}}">Edit</a>
               <form action="" method="post" style="display:inline">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-warning">Delete</button>
      
               </form>
               <a class="btn btn-success" href="">PDF</a>
                
              </td>
            </tr>
            @endforeach
    
    
          </tbody>
        </table>
    
      </div>
    
    </x-backend.layouts.master>
    