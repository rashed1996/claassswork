<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        $orders=Order::all();
        return view('backend/orders/orderlist',compact('orders'));
    }

    public function store(Request $request)
    {
        $order = Order::create([
            'email' => $request->email,
            'phone_no' => $request->phone,
            'shipping_address' => $request->shipping_address,
            'order_no' => 1,
            'payment_method' => 'COD',
            'ordered_by' => Auth::id()
        ]);

        $productDetailsInfo = [];
        for ($i = 0, $max = count($request->product_ids); $i < $max; $i++) {
            $product = Product::findOrFail($request->product_ids[$i]);
            $productDetailsInfo['product_id'] = $request->product_ids[$i];
            $productDetailsInfo['qty'] = $request->product_qty[$i];
            $productDetailsInfo['product_title'] = $product->title;
            $productDetailsInfo['unit_price'] = $product->price;
            $order->details()->create($productDetailsInfo);
        }

        Cart::where('added_by', Auth::id())->delete();

        return redirect()->route('orders.success');
    }

    public function edit($id)
    {
        $order=Order::findOrFail($id);
        return view('backend/orders/orderedit',compact('order'));
    }

    public function update(Request $request, $id)
    {

        $order=Order::findOrFail($id);
        $order->update([
            'status' => $request->order_status,
        ]);
        return redirect()->route('order-list')->withMessage("Successfully Upadeted Order");
    }

    public function pdf()
    {
        $orders = Order::latest()->get();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('products.pdf');
    }

    public function success()
    {
        return view('order-success');
    }
}
