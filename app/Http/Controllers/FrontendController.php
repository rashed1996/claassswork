<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function welcome($categoryId = null)
    {
        if ($categoryId) {
            // $category = Category::findOrFail($categoryId);
            // $products = $category->products;
            $products = Product::where('category_id', $categoryId)->orderBy('id', 'desc')->paginate(9);
        } else {
            $products = Product::orderBy('id', 'desc')->paginate(9);
        }

        return view('welcome', compact('products'));
    }

    public function show(Product $product)
    {
        return view('product-details', compact('product'));
    }
}
