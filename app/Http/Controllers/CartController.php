<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function store(Request $request, Product $product)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $data['added_by'] = Auth::id();

        $data['product_id'] = $product->id;
        $data['unit_price'] = $product->price;
        $data['qty'] = $request->qty;

        Cart::create($data);

        return redirect()->back();
    }

    public function shopingBag()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $cartItems = Cart::where('added_by', Auth::id())->get();

        return view('cart', compact('cartItems'));
    }
}
