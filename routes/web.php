<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Mail\OrderCreated;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('send-mail', function(){

    $user = User::first();
    $product = Product::first();

    \Mail::to('mdahosanhabib@outlook.com')->send(
        new OrderCreated($user, $product)
    );
});

Route::get('products/{product}', [FrontendController::class, 'show'])->name('single-product');
Route::post('products/{product}/comment', [CommentController::class, 'store'])->name('product-comment');
Route::post('products/{product}/add-to-cart', [CartController::class, 'store'])->name('update-cart');
Route::get('/shoping-bag', [CartController::class, 'shopingBag'])->name('shoping-bag');
Route::post('/orders/create', [OrderController::class, 'store'])->name('orders.create');
Route::get('/orders/success', [OrderController::class, 'success'])->name('orders.success');

// Update: PUT/PATCH
// Delete: DELETE

Route::prefix('admin')->middleware('auth')->group(function () {
   
    Route::get('/deleted-products', [ProductController::class, 'trash'])->name('products.trash');
    Route::get('/deleted-products/{id}/restore', [ProductController::class, 'restore'])->name('products.restore');
    Route::delete('/deleted-products/{id}', [ProductController::class, 'delete'])->name('products.delete');
    Route::get('/products/download-pdf', [ProductController::class, 'pdf'])->name('products.pdf');

    // Route::get('/products', [ProductController::class, 'index'])->name('products.index');
    // Route::get('/products/create', [ProductController::class, 'create'])->name('products.create');
    // Route::post('/products/store', [ProductController::class, 'store'])->name('products.store');
    // Route::get('/products/{product}', [ProductController::class, 'show'])->name('products.show');
    // Route::get('/products/{product}/edit', [ProductController::class, 'edit'])->name('products.edit');
    // Route::patch('/products/{product}', [ProductController::class, 'update'])->name('products.update');
    // Route::delete('/products/{product}', [ProductController::class, 'destroy'])->name('products.destroy');

    Route::resource('products', ProductController::class);
    Route::get('orders',[OrderController::class,'index'])->name('order-list');
    Route::get('orders/{id}/edit',[OrderController::class,'edit'])->name('order-edit');
    Route::patch('orders/{id}',[OrderController::class,'update'])->name('order.update');


    Route::get('my-profile', function(){
        dd(auth()->user()->profile);
    })->name('my-profile');

});

require __DIR__.'/auth.php';

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');


Route::get('/dashboard', function () {
    return view('backend.dashboard');
})->middleware('checkAgeMiddleware');

Route::get('/categories/{categoryId?}', [FrontendController::class, 'welcome'])->name('category-wise-product');

Route::get('/', [FrontendController::class, 'welcome'])->name('welcome');

Route::get('/table', function () {
    return view('backend.table');
});


